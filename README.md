# sexpr
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mae.earth/pkg/sexpr)](https://goreportcard.com/report/gitlab.com/mae.earth/pkg/sexpr)

Symbolic-expression parser in golang. 

```bash
go get gitlab.com/mae.earth/pkg/sexpr
```

```go
root,_ := ParseString("(+ 1 2)",nil)
out,_ := OutputString(root)
fmt.Printf("%s",out)
```

```bash
(+ 1 2)
```


```go
package main

import (
	"fmt"
	"gitlab.com/mae.earth/pkg/sexpr"
	"gitlab.com/mae.earth/pkg/sexpr/cell"
)

type value string

func (v value) String() string {
	return fmt.Sprintf("%s", string(v))
}

func main() {

	c := cell.New(cell.List(value("+"), value("1"), value("2")),nil)

	str,err := sexpr.OutputString(c)
	if err != nil {
		panic(err)
	}

	root,err := sexpr.ParseString(str,nil)
	if err != nil {
		panic(err)
	}

	if root.IsList() {
		list := root.List()
		fmt.Printf("%d atoms in the list\n",cell.Count(list))

		head := cell.First(list)
		fmt.Printf("operation %s\n",head.Value())
	
		rest := cell.Rest(list)

		for {
			if rest == nil {
				break
			}
			fmt.Printf("\t%s\n",rest.Value())

			rest = rest.Next()	
		}
		
	}
}
```

```bash
3 atoms in the list
operation +
	1
	2
```



