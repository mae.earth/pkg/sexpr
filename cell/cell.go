/* mae.earth/pkg/sexpr/cell/cell.go */
package cell

import (
	"fmt"
	"strings"
)

const (
	Empty = ""
)

/* Stringer */
type Stringer interface {
	String() string
}

/* Compact */
func Compact(c *Cell) string {
	if c == nil {
		return "nil"
	}
	s := c.String()
	return strings.Replace(strings.Replace(strings.Replace(s, "\n", "", -1), "\t", "", -1), " ", "", -1)
}

/* Cell struct for placing two atoms into */
type Cell struct {
	h, t  Stringer    /* head & tail, car & cdr, first & rest ... */
	u     interface{} /* user-data, useful for high-level parser annotation */
	count int
}

/* String */
func (c *Cell) String() string {
	if c == nil {
		return "nil"
	}

	h := "empty"
	t := "empty"

	if c.h != nil {
		h = c.h.String()
	}
	if c.t != nil {
		t = c.t.String()
	}

	return fmt.Sprintf("cell{\n\t%s + \n\t%s\n}", h, t) /* TODO: formating needs work, maybe use tree? */
}

/* Set the user information */
func (c *Cell) Set(u interface{}) *Cell {
	c.u = u
	return c
}

/* Get the user information that is set */
func (c *Cell) Get() interface{} {
	return c.u
}

/* Copy not a deep copy of contents */
func (c *Cell) Copy() *Cell {
	n := New(c.h, nil)
	n.u = c.u
	return n
}

/* IsEmpty */
func (c *Cell) IsEmpty() bool {
	return (c.h == nil && c.t == nil)
}

/* IsList */
func (c *Cell) IsList() bool {
	if c.h == nil {
		return false
	}
	_, ok := c.h.(*Cell)
	return ok
}

/* IsValue */
func (c *Cell) IsValue() bool {
	return (c.IsList() == false)
}

/* Value */
func (c *Cell) Value() Stringer {
	return c.h
}

/* ToSValue */
func (c *Cell) ToSValue() (string, bool) {
	ok := c.IsValue()
	if !ok {
		return "", false
	}
	if c.h == nil {
		return "", false
	}
	return c.h.String(), ok
}

/* SValue */
func (c *Cell) SValue() string {
	if c.IsValue() {
		return c.h.String()
	}
	return ""
}

/* List */
func (c *Cell) List() *Cell {
	if c.h == nil {
		return nil
	}
	if a, ok := c.h.(*Cell); ok {
		return a
	}
	return nil
}

/* Next */
func (c *Cell) Next() *Cell {
	if c.t == nil {
		return nil
	}
	if a, ok := c.t.(*Cell); ok {
		return a
	}
	return nil
}

/* Open */
func Open(cell *Cell) *Cell {
	return &Cell{h: cell, t: nil}
}

/* New */
func New(h, t Stringer) *Cell {
	return &Cell{h: h, t: t}
}
